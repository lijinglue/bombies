﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace Bombies
{
    public class Player : NetworkBehaviour
    {

        const string opponentLayerNameSuffix = "Destructable";

        public GameObject swordPrefab;

        public GameObject[] boombiePrefabs;

        [SerializeField] 
        float m_projectileLifeTime = 8;
    
        [SyncVar(hook = "OnUpdatePlayerTag")]
        public string playerTag;

        [SyncVar]
        public int order;


        [SyncVar]
        Vector3 playerProjectSpawnPosition;

        private string token;

        //Only on server
        private static List<string> playerTokens = new List<string>();


        public float Stamina { get { return m_Stamina; } }

        public float Health { get { return m_health; } }

        public Boolean IsDead { get { return m_health <= 0; } }

        public float ShotCooldown
        {
            get
            {
                var elapsed = Time.time - m_localLastShootTime;
                return elapsed > m_ShootIntervalSec ? 0 : m_ShootIntervalSec - elapsed;
            }
        }

        public float BombieCoolDown
        {
            get
            {
                var elapsed = Time.time - m_localLastBombieTime;
                return elapsed > m_BoombieIntervalSec ? 0 : m_BoombieIntervalSec - elapsed;
            }
        }

        [SerializeField] float m_MaxStamina;
        [SerializeField] float m_MaxHealth = 20;
        [SerializeField] float m_ShootStaminaCost;
        [SerializeField] float m_BoombieStaminaCost;
        [SerializeField] float m_ShootIntervalSec;
        [SerializeField] float m_BoombieIntervalSec;
        [SerializeField] float m_StaminaRegenPerSec;
        [SerializeField] List<Vector3> m_ProjectileSpawnPoisitions;

        [SyncVar] float m_Stamina;
        [SyncVar] float m_health;
        [SyncVar] float m_lastShootTime = 0;
        [SyncVar] float m_lastBombieTime = 0;

        float m_localLastShootTime = 0;
        float m_localLastBombieTime = 0;

        int m_hitLayerMask;
        int m_destructableMask;

        private LocalPlayerRef m_localPlayerRef;

        public override void OnStartLocalPlayer()
        {
            if (string.IsNullOrEmpty(playerTag))
            {
                Debug.Log("starting local, requesting player identity");
                var token = Guid.NewGuid().ToString();
                CmdRequestPlayerTag(token);
            }
        }

        public override void OnStartAuthority()
        {
            GameObject.FindObjectOfType<LocalPlayerRef>().player = this.gameObject;
        }

        void Start()
        {
            m_localPlayerRef = GameObject.FindObjectOfType<LocalPlayerRef>();
            m_hitLayerMask = 1 << LayerMask.NameToLayer("Platform");
            m_Stamina = m_MaxStamina;
            m_health = m_MaxHealth;
            if (!this.hasAuthority)
            {
                GameObject.FindObjectOfType<LocalPlayerRef>().opponent = this.gameObject;
            }
        }

        void Update()
        {
            RegenStamina();
            if ((isClient && !hasAuthority) || IsDead)
            {
                return;
            }


            if (Input.GetButtonUp("Fire1"))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit raycastHit;
                if (Physics.Raycast(ray, out raycastHit, 500, m_destructableMask))
                {
                    CmdThrowKnife(raycastHit.point);
                    m_localLastShootTime = Time.time;
                }
                else if (Physics.Raycast(ray, out raycastHit, 500, m_hitLayerMask) && m_localPlayerRef.opponent != null)
                {
                    CmdThrowBombie(raycastHit.point,
                        m_localPlayerRef.opponent.GetComponent<NetworkIdentity>().netId);
                    m_localLastBombieTime = Time.time;
                }
            }
        }

        void RegenStamina()
        {
            if (isServer)
            {
                m_Stamina += m_StaminaRegenPerSec * Time.deltaTime;
                if (m_Stamina > m_MaxStamina)
                {
                    m_Stamina = m_MaxStamina;
                }
            }
        }

        public void Reset()
        {
            m_Stamina = m_MaxStamina;
            m_health = m_MaxHealth;
        }

        [Command]
        void CmdThrowBombie(Vector3 dest, NetworkInstanceId opponentId)
        {
            #region check stamina and Cd
            if (Stamina < m_BoombieStaminaCost)
            {
                Debug.Log("not enough resource for bombie");
                return;
            }

            if (Time.time - m_lastBombieTime < m_BoombieIntervalSec)
            {
                Debug.Log("cooling down for bombie" + (Time.time - m_lastBombieTime));
                return;
            }

            m_Stamina -= m_BoombieStaminaCost;
            m_lastBombieTime = Time.time;
            #endregion
            var bombie = Instantiate(boombiePrefabs[order], dest, Quaternion.identity) as GameObject;
            bombie.transform.Rotate(180f * Vector3.up);
            bombie.layer = LayerMask.NameToLayer(playerTag + "Destructable");
            bombie.tag = playerTag;
            if (m_localPlayerRef.opponent != null)
            {
                bombie.GetComponent<Bombie>().opponentId = opponentId;
            }
            NetworkServer.Spawn(bombie);
            RpcSetLayerAndTag(
                bombie.GetComponent<NetworkIdentity>().netId,
                bombie.tag,
                bombie.gameObject.layer);
        }

        [Command]
        void CmdThrowKnife(Vector3 dest)
        {
            if (m_Stamina < m_ShootStaminaCost ||
                Time.time - m_lastShootTime < m_ShootIntervalSec)
            {
                return;
            }
            m_Stamina -= m_ShootStaminaCost;
            m_lastShootTime = Time.time;

            GameObject projectile = Instantiate(swordPrefab,
                                        playerProjectSpawnPosition,
                                        Quaternion.identity) as GameObject;

            projectile.layer = LayerMask.NameToLayer(playerTag + "Objects");
            projectile.tag = playerTag;
            NetworkServer.Spawn(projectile);

            RpcSetLayerAndTag(
                projectile.GetComponent<NetworkIdentity>().netId,
                projectile.tag,
                projectile.layer);

            projectile.GetComponent<Sword>().TargetPosition = dest;
            Destroy(projectile, m_projectileLifeTime);
        }

        [ClientRpc]
        void RpcSetLayerAndTag(NetworkInstanceId netId, string tag, int layer)
        {
            var obj = ClientScene.FindLocalObject(netId);
            Debug.Log("Setting layer and tag for:" + obj);
            obj.tag = tag;
            obj.layer = layer;
        }

        [Command]
        void CmdRequestPlayerTag(string token)
        {
            if (playerTokens.Count < 2)
            { // accept player
                var playerIndex = playerTokens.IndexOf(token);
                if (playerIndex > -1)
                { // connected before
                    this.playerTag = "P" + (playerIndex + 1).ToString();
                    order = playerIndex;
                }
                else
                { // new player
                    playerTokens.Add(token);
                    this.playerTag = "P" + playerTokens.Count;
                    order = playerTokens.Count - 1;
                }
                this.playerProjectSpawnPosition = m_ProjectileSpawnPoisitions[order];
            }
        }

        [Command]
        public void CmdTakeDamage(float damage)
        {
            if (m_health > 0)
            {
                m_health -= damage;
            }
        }

        void OnUpdatePlayerTag(string playerTag)
        {
            this.playerTag = playerTag;
            this.name = playerTag;
            var destructableMaskName = "";
            switch (playerTag)
            {
                case("P1"):
                    destructableMaskName = "P2" + opponentLayerNameSuffix;
                    break;
                case("P2"):
                    destructableMaskName = "P1" + opponentLayerNameSuffix;
                    break;
            }
            m_destructableMask = 1 << LayerMask.NameToLayer(destructableMaskName);
        }
    }
}
