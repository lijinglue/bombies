﻿using UnityEngine;
using UnityEngine.Networking;

public class Sword : NetworkBehaviour
{

    [SyncVar(hook = "OnTargetUpdate")]
    Vector3 m_TargetPosition;

    public Vector3 TargetPosition { set { m_TargetPosition = value; } }

    [SerializeField] float m_Speed = 20;

    public AudioSource m_SpawnSound;
    public AudioSource m_CollisionSound;

    void Start()
    {
        m_SpawnSound.Play(0);
    }

    [SyncVar]
    private bool isScheduledForDestroy = false;

    void Update()
    {
        if (isScheduledForDestroy)
        {
            this.transform.position = this.transform.position + this.transform.forward.normalized / 5;
        }
    }

    void OnTargetUpdate(Vector3 position)
    {
        this.m_TargetPosition = position;
        this.transform.LookAt(position);
        this.GetComponent<Rigidbody>().velocity = this.transform.forward.normalized * m_Speed;
    }

    void OnCollisionEnter(Collision collision)
    {
        var bombie = collision.collider.GetComponent<Bombie>();
        if (tag != collision.collider.tag && bombie && isServer)
        {
            bombie.Defuse();
            Destroy(this.gameObject);
        }
        else
        {
            ExecuteOnAll("OnHitUndestructable");
        }
    }

    void ExecuteOnAll(string method)
    {
        if (isServer)
        {
            Invoke(method, 0);
            RpcExecuteOnClient(method);
        }
    }

    [ClientRpc]
    void RpcExecuteOnClient(string method)
    {
        Invoke(method, 0);
    }

    void OnHitUndestructable()
    {
        var rg = this.GetComponent<Rigidbody>();
        rg.velocity = Vector3.zero;
        rg.drag = 0;
        rg.isKinematic = true;
        m_CollisionSound.Play(0);
        if (isServer)
        {
            Invoke("ScheduleDestory", 0.3f);
        }
    }

    private void ScheduleDestory()
    {
        isScheduledForDestroy = true;
        Destroy(this.gameObject, 2);
    }
}
