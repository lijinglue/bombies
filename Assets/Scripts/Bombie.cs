using UnityEngine;
using BeautifulDissolves;
using UnityEngine.Networking;

[RequireComponent(typeof(Dissolve))]
public class Bombie : NetworkBehaviour {

    [HideInInspector]
    [SyncVar]
    public NetworkInstanceId opponentId;

    [SerializeField] GameObject m_ExplosionVfx;
    [SerializeField] float m_DetonateDelay;
    [SerializeField] float m_Damage;
    [SerializeField] Animator m_Animator;
	[SerializeField] AudioSource m_Audio;
	[SerializeField] ParticleSystem m_DeathParticles;
	[SerializeField] float m_removeCorpseDelay = 3f;
	[SerializeField] float m_vfxLifeTime = 1f;
    private LocalPlayerRef m_localPlayerRef;

    [SyncVar(hook="OnDefusedUpdate")]
    private bool m_IsDefused = false;


    void Start() {
        m_localPlayerRef = GameObject.FindObjectOfType<LocalPlayerRef>();
        Invoke("Detonate", m_DetonateDelay);
    }

	public void DestroySelf()
	{
        this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;
        this.gameObject.GetComponent<SphereCollider>().enabled = false;
		Destroy(this.gameObject, 1);
	}

    public void Defuse() {
        m_IsDefused = true;
        OnDefused();
    }

    public void OnDefused() {
        this.GetComponent<Collider>().enabled = false;
		m_Animator.SetTrigger("Dead");
        this.GetComponent<Dissolve>().TriggerDissolve();
        m_Audio.Play();
		m_DeathParticles.Play();
        if(isServer) {
            Destroy(this.gameObject, m_removeCorpseDelay);
        }
    }

    public void OnDefusedUpdate(bool isDefused) {
        m_IsDefused = isDefused;
        OnDefused();
    }

    public void Detonate() {
        if(m_IsDefused) {
            return;
        }
        if (m_localPlayerRef.player.GetComponent<NetworkIdentity>().netId == opponentId)
        {
            m_localPlayerRef.player.GetComponent<Bombies.Player>().CmdTakeDamage(m_Damage);
        }
        var evfx = Instantiate(m_ExplosionVfx, this.transform.position, Quaternion.identity) as GameObject;
        Destroy(evfx, m_vfxLifeTime);
        DestroySelf();
    }
}
