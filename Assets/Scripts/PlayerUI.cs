﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerUI : NetworkBehaviour
{
    [SerializeField]
    public LocalPlayerRef localPlayerRef;

    public Text healthText;
    public Text staminaText;
    public Text shootCooldownText;
    public Text bombieCooldownText;
    public Text winLostText;
    public GameObject winLostGroup;

    void OnGUI()
    {
        if (!localPlayerRef.player || !localPlayerRef.opponent)
        {
            return;
        }
        var player = localPlayerRef.player.GetComponent<Bombies.Player>();
        var opponent = localPlayerRef.opponent.GetComponent<Bombies.Player>();
        healthText.text = "HP:" + player.Health;
        if (player.IsDead)
        {
            winLostText.text = "Defeat!";
            winLostGroup.SetActive(true);
            return;
        }
        else if (opponent.IsDead)
        {
            winLostText.text = "Victory!";
            winLostGroup.SetActive(true);
            return;
        }
        else
        {
            winLostGroup.SetActive(false);
        }
        this.GetComponent<Text>().text = player.playerTag;
        staminaText.text = "ST:" + player.Stamina;
        shootCooldownText.text = "SCd:" + player.ShotCooldown;
        bombieCooldownText.text = "GCd:" + player.BombieCoolDown;
    }

    public void Reset()
    {
        if (isServer)
        {
            var player = localPlayerRef.player.GetComponent<Bombies.Player>();
            var opponent = localPlayerRef.opponent.GetComponent<Bombies.Player>();
            player.Reset();
            opponent.Reset();
        }
    }
}
