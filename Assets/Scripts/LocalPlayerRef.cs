﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalPlayerRef : MonoBehaviour
{
    public GameObject player;
    public GameObject opponent;

    void Awake()
    {
        DontDestroyOnLoad(this.transform.gameObject);
    }
}
