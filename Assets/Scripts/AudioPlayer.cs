﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {
	public AudioClip clip;
	// Use this for initialization
	void Start () {
		AudioSource.PlayClipAtPoint(clip, transform.position);
	}
}
